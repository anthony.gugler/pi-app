import React, { useState } from "react";
import { Button, Card, Elevation } from "@blueprintjs/core";

const MapComponent = () => {
  const [buttons, setButtons] = useState([
    {
      name: "Weather Map",
      address:
        "https://map.geo.admin.ch/embed.html?layers=ch.meteoschweiz.mess" +
        "werte-windgeschwindigkeit-kmh-10min&lang=en&topic=ech&bgLayer=ch.swisstopo.pixelkarte-farbe",
    },
    {
      name: "Forest Damage Map",
      address:
        "https://map.geo.admin.ch/embed.html?layers=ch.bafu.waldschaden-200" +
        "9&lang=en&topic=ech&bgLayer=ch.swisstopo.pixelkarte-farbe&layers_opacity=1",
    },
    {
      name: "Wildlife Map",
      address:
        "https://map.geo.admin.ch/embed.html?layers=ch.bfs.gebaeude_wohnungs" +
        "_register,ch.bafu.flugwildruhezonen_kernzonen,ch.bafu.fauna-vernetzun" +
        "gsachsen_national&lang=en&bgLayer=ch.swisstopo.pixelkarte-farbe&catalog" +
        "Nodes=214,219,220&layers_visibility=false,false,true&layers_opacity=0.75,0.75,0.75",
    },
  ]);
  const [currentAddressIndex, setCurrentAddressIndex] = useState(0);

  const handleButtonClick = (index) => {
    setCurrentAddressIndex(index);
  };

  const buttonList = buttons.map((button, index) => (
    <Button
      key={index}
      onClick={() => handleButtonClick(index)}
      intent="primary"
      style={{
        color: "black",
        borderRadius: "3px",
        padding: "10px 20px",
        fontWeight: "bold",
        margin: "10px",
        transition: "background-color 0.2s ease-in-out",
      }}
      onMouseEnter={(e) => {
        e.target.style.backgroundColor = "#3E3E3E";
      }}
      onMouseLeave={(e) => {
        e.target.style.backgroundColor = "transparent";
      }}
    >
      {button.name}
    </Button>
  ));

  return (
    <Card
      elevation={Elevation.TWO}
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        padding: "20px",
      }}
    >
      <iframe
        src={buttons[currentAddressIndex].address}
        width="600"
        height="450"
        style={{ border: "none", borderRadius: "10px", margin: "20px 0" }}
        allow="geolocation"
      ></iframe>
      <div style={{ display: "flex", justifyContent: "center" }}>
        {buttonList}
      </div>
    </Card>
  );
};

export default MapComponent;
