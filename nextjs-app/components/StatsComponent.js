import React from "react";
import { Card, Icon, NonIdealState } from "@blueprintjs/core";

const StatsComponent = ({ stats }) => {
  return (
    <Card
      style={{
        display: "flex",
        justifyContent: "space-evenly",
        margin: "20px 0",
      }}
    >
      {stats.map((stat) => (
        <Card key={stat.label} style={{ textAlign: "center", width: "20%" }}>
          <Icon icon={stat.icon} style={{ margin: "20px 0" }} />
          <div>
            <div style={{ fontWeight: "bold" }}>{stat.label}</div>
            <div>{stat.value}</div>
          </div>
        </Card>
      ))}
      {stats.length === 0 && (
        <NonIdealState
          title="No Stats Available"
          description="No stats data has been provided."
          icon={<Icon icon="info-sign" />}
        />
      )}
    </Card>
  );
};

export default StatsComponent;
