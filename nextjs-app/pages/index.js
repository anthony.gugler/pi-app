import React from 'react';
import MapComponent from '../components/MapComponent';
import StatsComponent from '../components/StatsComponent';
import { H1 } from "@blueprintjs/core";

const IndexPage = () => {
const stats = [
{ icon: 'fas fa-bolt', label: 'Production', value: '10 000 kWh' },
{ icon: 'fas fa-solar-panel', label: 'Solaire', value: '5 000 kWh' },
{ icon: 'fas fa-wind', label: 'Éolien', value: '3 000 kWh' },
{ icon: 'fas fa-tint', label: 'Hydraulique', value: '2 000 kWh' },
{ icon: 'fas fa-fire', label: 'Combustible fossile', value: '4 000 kWh' },
];

return (
<div>
<H1 style={{ padding: '20px' }}>PI - Energie maintenant et en 2050 ?</H1>
<StatsComponent stats={stats} />
<MapComponent />
</div>
);
};

export default IndexPage;